//création constante 
const discord = require('discord.js')//importation module discord.js
const client = new discord.Client() // création constante client
const config = require('./config.json') //importation du doc config.json 
const handler = require("./handler") //importation du doc handler
const ytdl = require("ytdl-core"); //importation module ytdl-core


//fonction démarage du BOT
client.on("ready", () => { 
    console.log("ok");//confirmation démarage dans terminal 
    handler(client, config);//récupération constante client et constancte config ou il y a mon Token de connexion 
    client.user.setActivity(" salut 🙂 "); //fonction simplement faite pour ajjouter une activiter 
   //fonction pour modifier les élements de présences (Activity et status de connexion)
    client.user.setPresence({
        activity: {
            name: 'In Dev'
        },
        status: 'dnd' 
    })
    
    
  });
  


// fonction lecture musique Youtube
client.on("message", message => { //création fonction message 
    if(message.content.startsWith(config.prefix + "song")){ //récuperation du contenu d'un message 
        if(message.member.voice.channel){ //Si le message et reconnu par le module ytdl-core 
            message.member.voice.channel.join().then(connection =>{ //Le BOT rejoindra le salon vocal ou et l'utilisateur 
                 //déclarage variation args
                let args = message.content.split(" "); //prépare le contenu du message précedent
                message.reply("L'audio a démarrer"); //le BOT répondra cela 
                //déclarage variation dispatcher 
                let dispatcher = connection.play(ytdl(args[1], {quality: "highestaudio"})); //ce connecte au serveur ytdl pour ainsi démarer et lire l'audio Youtube 
                //fonction de finition de la commande 
                dispatcher.on("finish", () =>{ // récuperation variable dispatcher  pour lui signifier que C'est fini 
                    dispatcher.destroy(); // suppresion du dispatcher 
                    connection.disconnect(); //déconnexion du BOT du vocal 
                    message.reply("L'audio est terminer");//le BOT répondra cela 
                });
                //fonction pour montrer les erreurs lié au dispatcher 
                dispatcher.on("error", err => {
                    console.log("erreur de dispatcher : " + err.catch); //le BOT répondra cela 
                });
                //récupération du message d'erreur en cache et explication de celui-ci dans le salon discord
            }).catch(err =>{
                message.reply("erreur lors de la connexion : " + err.catch); //le BOT répondra cela 
            });
        }
        //sinon il faut ce connecté a un vocal 
        else{
            message.reply("vous n'êtes pas connecté en vocal."); //le BOT répondra cela 
        }
    }
});




//fonction de récupération du token de L'API discord 
client.login(config.token)  